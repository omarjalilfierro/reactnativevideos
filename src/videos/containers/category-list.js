import React, { Component } from 'react'
import { 
    View,
    FlatList
} from 'react-native';

import Empty from '../components/empty';
import Separator from '../../sections/components/horizontal-separator';
import category from '../components/category';
import Layout from '../components/category-list-layout'
import Category from '../components/category';
class CategoryList extends Component {

    keyExtractor = (item) => item.id.toString()
    renderEmpty = () => <Empty text="No hay sugerencias :c"/>
    itemSeparator = () => <Separator />

    renderItem = ({item}) => {
        return (
            <Category {...item}/>
        )
    }

    render(){
        return (
            <Layout title="Categories">
                <FlatList
                    horizontal
                    keyExtractor={this.keyExtractor}
                    data={this.props.list}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.itemSeparator}
                    ListEmptyComponent={this.renderEmpty}
                ></FlatList>
            </Layout>
        )
    }
}

export default CategoryList;