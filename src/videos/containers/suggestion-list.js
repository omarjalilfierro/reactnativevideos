import React, {Component} from 'react';
import {
    FlatList,
    Text
} from 'react-native';

import SuggestionsLayout from '../components/suggestion-list-layout';
import Empty from '../components/empty';
import Separator from '../components/vertical-separator';
import Suggestion from '../components/suggestion';


class SuggestionList extends Component {
    renderEmpty = () => <Empty text="No hay sugerencias :c"/>
    itemSeparator = () => <Separator />

    renderItem = ({item}) => {
        return (
            <Suggestion {...item}/>
        )
    }

    keyExtractor = (item) => item.id.toString()

    render() {
        
        return (
            
            <SuggestionsLayout
                title="Recomendado para ti"
            >
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.list}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.itemSeparator}
                    ListEmptyComponent={this.renderEmpty}
                ></FlatList>
            </SuggestionsLayout>
                  
        )
    }
}

export default SuggestionList;
