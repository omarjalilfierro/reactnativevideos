import React, { Component } from 'react';
import Video from 'react-native-video'
import {
    View,
    Text,
    StyleSheet,
    ActivityIndicator
} from 'react-native'

import moment from 'moment'

import Layout from '../components/player-layout'
import ControlLayout from '../components/control-layout'
import PlayPause from '../components/play-pause'
import ProgressBar from '../components/progress-bar'
import FullScreen from '../components/fullscreen'

class Player extends Component {
    state = {
        isLoading: true,
        isPaused: false,
        videoDuration: 0,
        currentTime: 0
    }

    playPause = () => {
        this.setState((oldState, oldProps) => {
            return {
                isPaused: !oldState.isPaused
            }
        })
    }

    onBuffer = ({ isBuffering }) => {
        this.setState({
            isLoading: isBuffering
        })
    }

    onFullScreen = () => {
        this.player.presentFullscreenPlayer();
    }
    
    onProgress = (payload) => {
        this.setState({
            currentTime: payload.currentTime
        })
    }

    onChangeFinished = payload => {
        this.player.seek(payload)
        this.setState({ isPaused: false, isLoading: false })
    }
    
    onChangeStarted = payload => {
        this.setState({ paused: true, isLoading: true })
    }
    

    render(){
        let currentTime = moment(this.state.currentTime * 1000).format('mm:ss')
        let totalTime = moment(this.state.videoDuration * 1000).format('mm:ss')

        return (
            <Layout
                loading={this.state.isLoading}
                video={
                    <Video
                        onProgress={this.onProgress}
                        source={{uri: 'https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4'}}
                        style={styles.video}
                        resizeMode="contain"
                        onBuffer={this.onBuffer}
                        paused={this.state.isPaused}
                    />
                }
                loader={
                    <ActivityIndicator size='large'/>
                }
                controls={
                    <ControlLayout>
                        <PlayPause 
                            onPress={this.playPause}
                            isPaused={this.state.isPaused}
                        /> 
                        <ProgressBar onChangeFinished={this.onChangeFinished} onChangeStarted={this.onChangeStarted} progress={this.state.currentTime} videoDuration={this.state.videoDuration} />
                        <Text style={styles.progressTime}>{currentTime} / {totalTime}</Text>
                        <FullScreen onFullScreen={this.onFullScreen} />
                    </ControlLayout>
                }
            >
            </Layout>
        )
        
    }
}

const styles = StyleSheet.create({
    video: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },
    progressTime: {
      color: 'white',
      fontSize: 10,
      marginLeft: 5
    }
})

export default Player;