import React from 'react';
import { 
    View,
    Text,
    Image,
    StyleSheet,
    SafeAreaView,
} from 'react-native';


function Header(props) {
    return (
        <SafeAreaView style={styles.container}>
            <Image style={styles.logo}source={require('../../../assets/logo.png')}/>
            <View style={styles.right}>
                {props.children}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    logo: {
        width: 80,
        height: 26,
        resizeMode: 'contain',
    },
    container: {
        padding: 10,
        flexDirection: 'row',
    },
    right: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    }
})

export default Header;